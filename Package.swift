// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "GoogleInteractiveMediaAds",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        
        .library(name: "GoogleInteractiveMediaAds-iOS", targets: [
            "GoogleInteractiveMediaAds-iOS"
        ]),
        .library(name: "GoogleInteractiveMediaAds-tvOS", targets: [
            "GoogleInteractiveMediaAds-tvOS"
        ]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        
        .binaryTarget(name: "GoogleInteractiveMediaAds-iOS", path: "ios/GoogleInteractiveMediaAds.xcframework"),
        .binaryTarget(name: "GoogleInteractiveMediaAds-tvOS", path: "tvos/GoogleInteractiveMediaAds.xcframework"),
    ]
)
